======================
Koha 18.05 Manual (en)
======================

:Author: The Koha Community

.. include:: images.rst
.. toctree::
   :maxdepth: 2


   intro
   installation
   systempreferences
   administration
   tools
   patrons
   circulation
   cataloging
   course_reserves
   serials
   acquisitions
   lists
   reports
   opac
   searching
   plugins
   implementation_checklist
   cron_jobs
   webservices
   miscellaneous
   extending_koha
   links
   faq
   license

  
